#!/bin/bash
set -e

# usage: using a reference copy of RunCollatz in this directory,
# pass the name of the owner of the inbound merge request to test their outputs
# e.g. ./collatz-merge-check.sh theBrianCui
if [[ ! -e "./RunAllocator" ]]
then echo "./RunAllocator executable not found"
     exit 1
fi
chmod +x ./RunAllocator

if [[ "$1" == "" ]]
then echo "No Gitlab ID provided"
     exit 1
fi

GITLAB_ID="$1"
echo "Pulling merge request from ${GITLAB_ID}..."
REMOTE="git@gitlab.com:${GITLAB_ID}/cs371p-allocator-tests.git"
git fetch "$REMOTE" master
BRANCH="${GITLAB_ID}/cs371p-allocator-tests-master"
git checkout -B "$BRANCH" FETCH_HEAD

RIN="${GITLAB_ID}-RunAllocator.in"
ROUT="${GITLAB_ID}-RunAllocator.out"
TOUT="${GITLAB_ID}-RunAllocator.tmp"

if [[ ! -e "$RIN" || ! -e "$ROUT" ]]
then
    echo "FAIL: File names incorrect (could not find $RIN and $ROUT)"
    git checkout master
    exit 1
fi

COUNT=$(wc -l < "$RIN")
echo "Line Count: $COUNT"
if (( "$COUNT" < 100 || "$COUNT" > 200 ));
then
    echo "FAIL: Acceptance test line count  not within [100, 200]"
    git checkout master
    exit 1
fi

CHECKTESTDATA=$(checktestdata ../TestAllocator.ctd "$RIN" 2>&1 || echo $?)
echo "$CHECKTESTDATA"
if [[ "$CHECKTESTDATA" != "testdata ok!" ]]
then
    echo "FAIL: checktestdata failed to validate $RIN"
    git checkout master
    exit 1
fi

FIRSTLINE=$(head -1 "$RIN")
echo "Test Count: $FIRSTLINE"
if (( "$FIRSTLINE" < 10 || "$FIRSTLINE" > 20 ))
then
    echo "FAIL: Acceptance test quantity not within [10, 20]"
    git checkout master
    exit 1
fi

echo "Running tests, please wait..."
./RunAllocator < "$RIN" > "$TOUT"
echo "Checking diff..."

DIFF=$(diff "$ROUT" "$TOUT" 2>&1 | cat -vet || echo $?)
# rm "$TOUT"
if [[ "$DIFF" != "" ]]
then
   echo "$DIFF"
   echo "FAIL: Reference solution output did not match expected output"
   git checkout master
   exit 1
fi

echo "PASS: Merging to master..."
git checkout master
git pull
git merge --no-ff "$BRANCH" --no-edit
git push origin master

echo "Merge complete."
